﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MovementEffects;

public class ChandelierScript : MonoBehaviour {
	[SerializeField] bool chandLit;
	Light lightSource;

	// Use this for initialization
	void Start () {
		lightSource = GetComponentInChildren<Light> ();
		Timing.RunCoroutine (ChandFlicker());
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.L)) {
			lightSource.intensity = Random.Range (.75f, 1.25f);
		}
	}

	IEnumerator<float> ChandFlicker () {
		float chandTimer = Random.Range (.1f, .2f);
		while (chandLit) {
			lightSource.intensity = Random.Range (.9f, 1.1f);
			yield return 0;
		}
	}
}
