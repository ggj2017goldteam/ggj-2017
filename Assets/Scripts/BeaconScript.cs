﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MovementEffects;

public class BeaconScript : MonoBehaviour {
    [SerializeField] SonarFx fx;
    [SerializeField] float lengthOfBurst = 2f;

    private void OnEnable () {
        Timing.RunCoroutine (StartPulse ());
    }

	public IEnumerator<float> StartPulse () {
        fx.enabled = true;
        fx.origin = transform.position;
        yield return Timing.WaitForSeconds (lengthOfBurst);
		fx.enabled = false;
    }
}
