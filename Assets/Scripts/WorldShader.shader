﻿Shader "WorldShader" {
	Properties{
		_MainTex("Texture", 2D) = "white" {}
		_HighlightColor ("HighlightColor", Color) = (0,0,0,0)
		_Center("Center", Vector) = (0,0,0,0)
		_Radius("Radius", Float) = 0.5
	}

	SubShader{
		Tags{
			"RenderType" = "Opaque"
		}

		CGPROGRAM
		#pragma surface surf Standard

		struct Input {
			float4 color : COLOR;
			float2 uv_MainTex;
			float3 worldPos;
		};

		float3 _HighlightColor;
		sampler2D _MainTex;
		float3 _Center;
		float _Radius;

		void surf(Input IN, inout SurfaceOutputStandard o) {
			float d = distance(_Center, IN.worldPos);
			float dN = 1 - saturate(d / _Radius);
			if (dN > 0.25 && dN < 0.3)
				o.Albedo = _HighlightColor;
			else {
				half4 c = tex2D(_MainTex, IN.uv_MainTex);
				o.Albedo = c.rgb;
			}
				
		}
	ENDCG
	}
	Fallback "Diffuse"
}