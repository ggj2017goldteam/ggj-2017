﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEnd : MonoBehaviour {

    [SerializeField] GameObject gameOver, gameWin;

    private void Start () {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        if (Constants.HasBook) {
            WinGame ();
        } else {
            EndGame ();
        }
    } 

    void EndGame () {
        gameOver.SetActive (true);
    }

    void WinGame () {
        gameWin.SetActive (true);
    }

    public void EndConfirm () {
        SceneManager.LoadScene ("Menu");
    }

    public void EndCancel () {
        Application.Quit ();
    }
}
