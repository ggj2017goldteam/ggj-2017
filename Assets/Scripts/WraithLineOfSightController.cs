﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MovementEffects;

public class WraithLineOfSightController : MonoBehaviour {

    WraithController controller;

    public float baseSightLength, baseSightRadius;

    private void OnEnable () {
        if (controller == null)
            controller = GetComponentInParent<WraithController> ();

        controller.sightLength = baseSightLength*10;
        controller.sightRadius = baseSightRadius;

        SetSightConeSize ();
    }

    public void SetSightConeSize () {
        transform.localScale = new Vector3 (controller.sightRadius, controller.sightLength, controller.sightRadius);
    }

    private void OnTriggerEnter (Collider other) {
        if (other.gameObject.CompareTag("Player")) {
            RaycastHit hit;
            if (Physics.Linecast (Camera.main.transform.position, other.gameObject.transform.position, out hit)) {
                if (hit.collider.gameObject.CompareTag ("Player")) {
                    controller.playerSpotted = true;
                    controller.CurrentState = WraithController.WraithState.Attacking;
                    Timing.RunCoroutine(controller.StartAttacking ());
                    controller.gameObject.GetComponent<EnemySounds> ().AttackWail ();
                }   
            }
        }
    }

    private void OnTriggerStay (Collider other) {
        if (other.gameObject.CompareTag ("Player")) {
            RaycastHit hit;
            if (Physics.Linecast (Camera.main.transform.position, other.gameObject.transform.position, out hit)) {
                if (!hit.collider.gameObject.CompareTag ("Player")) {
                    
                }
            }
        }
    }

    private void OnTriggerExit (Collider other) {
        if (other.gameObject.CompareTag ("Player")) {
            RaycastHit hit;
            if (Physics.Linecast (Camera.main.transform.position, other.gameObject.transform.position, out hit)) {
                if (hit.collider.gameObject.CompareTag ("Player")) {
                    controller.CurrentState = WraithController.WraithState.Searching;
                    Timing.RunCoroutine (controller.StartSearching ());
                    Timing.RunCoroutine (controller.gameObject.GetComponent<EnemySounds> ().PassiveWails (), "PassiveWails");
                }
            }
        }
    }
}
