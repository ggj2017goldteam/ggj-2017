﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MovementEffects; 

public class LightningScript : MonoBehaviour {
	[SerializeField] Light lightning;
	float minInensity;
	float maxIntensity;
	GameObject playerObj;
	Camera playerCam;
	Color normalSky;
	Color[] lightningSky;
	[SerializeField] float lightningTimerConst;
	[SerializeField] float lightningTimerRandInt;
	[SerializeField] float lightningWaitTimerConst;
	[SerializeField] float lightningWaitTimerRandInt;
	[SerializeField] AudioClip[] thunderSound;
	AudioSource thunderSource;


	// Use this for initialization
	void OnEnable () {
		thunderSource = GetComponent<AudioSource> ();
		playerObj = GameObject.FindGameObjectWithTag ("Player");
		playerCam = playerObj.GetComponentInChildren<Camera> ();
		normalSky = new Color32 (0x2D, 0x2D, 0x2D, 0x05);
		//normalSky = new Color32 (0x04, 0x0D, 0x1C, 0x05);  //"050A1305"; colored sky
		lightningSky = new Color[] {new Color32 (0xC9, 0xC9, 0xC9, 0x05), new Color32 (0xA5, 0xA5, 0xA5, 0x05), new Color32 (0xCC, 0xCC, 0xCC, 0x05)}; //C9C9C905
		//lightningSky = new Color[] {new Color32 (0xC9, 0xC9, 0xC9, 0x05), new Color32 (0x96, 0xA5, 0xBF, 0x05), new Color32 (0xBE, 0xCC, 0xE5, 0x05)}; //C9C9C905 Color
		lightning = GetComponentInChildren<Light> ();
		minInensity = .2f;
		maxIntensity = 2;
		lightning.intensity = minInensity;
		Camera.main.backgroundColor = normalSky;
		Timing.RunCoroutine (LightningWait ());
	}


	IEnumerator<float> LightningWait () {
		float lightningWaitTimer = lightningWaitTimerConst + Random.Range (lightningWaitTimerRandInt / -2f, lightningWaitTimerRandInt / 2f);
		while (lightningWaitTimer > 0) {
			lightningWaitTimer -= Time.deltaTime;
			yield return 0f;
		} 
		Timing.RunCoroutine (LightningStrike ());	
	}

	IEnumerator<float> LightningStrike () {
        if (lightning == null)
            yield break;

        float lightningTimer = lightningTimerConst + Random.Range (lightningTimerRandInt / -2f, lightningTimerRandInt / 2f);
		int seqNum = 0;
		lightning.intensity = maxIntensity;
        Camera.main.backgroundColor = lightningSky [0];
		while (lightningTimer > 0) {
			lightningTimer -= Time.deltaTime;
			yield return 0f;
		}

		lightningTimer = lightningTimerConst + Random.Range (lightningTimerRandInt / -2f, lightningTimerRandInt / 2f);
		lightning.intensity -= .7f;
        Camera.main.backgroundColor = lightningSky [1];
		while (lightningTimer > 0) {
			lightningTimer -= Time.deltaTime;
			yield return 0f;
		}

		lightningTimer = lightningTimerConst + Random.Range (lightningTimerRandInt / -2f, lightningTimerRandInt / 2f);
		lightning.intensity += .4f;
        Camera.main.backgroundColor = lightningSky [2];
		while (lightningTimer > 0) {
			lightningTimer -= Time.deltaTime;
			yield return 0f;
		}
		lightning.intensity = minInensity;
        Camera.main.backgroundColor = normalSky;
		StartCoroutine (LightningWait ());
		StartCoroutine (ThunderSound ());
	}

	IEnumerator<float> ThunderSound () {
		float thunderTimer = 0.5f + Random.Range (-.3f, .3f);
		while (thunderTimer > 0) {
			thunderTimer -= Time.deltaTime;
			yield return 0f;
		}
		thunderSource.clip = thunderSound[Random.Range (0, 3)];
		thunderSource.Play();
	}

}
