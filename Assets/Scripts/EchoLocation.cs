﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;
using MovementEffects;

public class EchoLocation : MonoBehaviour {
    [SerializeField] GameObject beaconPrefab, gameOver, gameWin;
    Vector3 pos;

	void Update () {
        RaycastHit hit;
        if (Input.GetButtonDown ("Fire1") && Physics.Raycast(new Ray(Camera.main.transform.position, Camera.main.transform.rotation * Vector3.forward), out hit)) {
            GameObject obj = Instantiate (beaconPrefab, hit.point, Quaternion.identity) as GameObject;
            obj.name = "Beacon";
        }
    }

    private void OnCollisionEnter (Collision collision) {
        if (collision.collider.gameObject.CompareTag("Book")) {
            Constants.HasBook = true;
            Destroy (collision.gameObject);
            Timing.KillAllCoroutines ();
            SceneManager.LoadScene ("EndGame");
        }
    }
}
