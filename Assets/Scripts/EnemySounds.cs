﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MovementEffects;

public class EnemySounds : MonoBehaviour {

	[SerializeField] AudioClip[] passiveWails;
	[SerializeField] AudioClip attackWail;
	AudioSource currentSound;
    [SerializeField] float passiveTimerConst;
    [SerializeField] float passiveTimerRand;

    WraithController controller;
    WraithController.WraithState wraithState;

	void Start () {
        if (controller == null)
            controller = GetComponent<WraithController> ();

		wraithState = controller.CurrentState;
		currentSound = GetComponent<AudioSource>();
        Timing.RunCoroutine (PassiveWails (), "PassiveWails");
	}

	public IEnumerator<float> PassiveWails() {
		float timer;
        currentSound.spatialBlend = 1f;
        currentSound.clip = passiveWails[Random.Range (0, passiveWails.Length)];
		timer = currentSound.clip.length;
		currentSound.Play ();
		while (timer > 0) {
            if (controller.CurrentState == WraithController.WraithState.Idle || controller.CurrentState == WraithController.WraithState.Searching) {
                timer -= Time.deltaTime;
            } else {
                timer = 0;
            }
            yield return 0;
        }
        if (controller.CurrentState == WraithController.WraithState.Idle || controller.CurrentState == WraithController.WraithState.Searching) {
            timer = passiveTimerConst + Random.Range (passiveTimerRand / -2, passiveTimerRand / 2);
        }
        while (timer > 0) {
            if (controller.CurrentState == WraithController.WraithState.Idle || controller.CurrentState == WraithController.WraithState.Searching) {
                timer -= Time.deltaTime;
            } else {
                timer = 0;
            }
            yield return 0;
		}
        if (controller.CurrentState == WraithController.WraithState.Idle || controller.CurrentState == WraithController.WraithState.Searching) {
            Timing.RunCoroutine (PassiveWails (), "PassiveWails");
        }
    }

	public void AttackWail() {
        currentSound.spatialBlend = 0f;

		currentSound.Stop ();
		currentSound.clip = attackWail;
		currentSound.Play ();
	}
}
