﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using MovementEffects;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(NavMeshAgent))]
public class WraithController : MonoBehaviour {

    public enum WraithState { Searching, Idle, Alert, Patrol, Attacking }
    private WraithState currentState;

    private GameObject player;
    private NavMeshAgent agent;
    private WraithLineOfSightController lineOfSightController;

    [HideInInspector] public bool playerSpotted;
    private Vector3 lastKnownPlayerPosition;

    [SerializeField] private float alertRange;
    [SerializeField] private LayerMask alertLayerMask;

    [HideInInspector] public float sightLength, sightRadius;

    private void OnEnable () {
        if (player == null)
            player = GameObject.FindGameObjectWithTag ("Player");

        if (agent == null)
            agent = GetComponent<NavMeshAgent> ();

        if (lineOfSightController == null)
            lineOfSightController = GetComponentInChildren<WraithLineOfSightController> ();
    }

    void AlertNearbyWraiths () {
        Collider[] colliders = Physics.OverlapSphere (transform.position, alertRange, alertLayerMask);

        foreach (Collider collider in colliders) {
            WraithController wc = collider.gameObject.GetComponentInParent<WraithController> ();
            if (wc != null && wc != this) {
                wc.currentState = WraithState.Searching;
                Timing.RunCoroutine (wc.StartSearching ());
            }
        }
    }

    public IEnumerator<float> StartAttacking () {
        AlertNearbyWraiths ();

        sightLength = lineOfSightController.baseSightLength * 20f;
        sightRadius = lineOfSightController.baseSightRadius * 1.5f;
        lineOfSightController.SetSightConeSize ();

        while (currentState == WraithState.Attacking) {
            Attacking ();
            yield return 0f;
        }
    }

    void Attacking () {
        lastKnownPlayerPosition = player.transform.position;
        transform.LookAt (player.transform);
        agent.SetDestination (lastKnownPlayerPosition);
    }

    public IEnumerator<float> StartSearching () {
        while (transform.position != lastKnownPlayerPosition) {
            yield return 0f;
        }

        sightLength = lineOfSightController.baseSightLength * 2f;
        sightRadius = lineOfSightController.baseSightRadius;
        lineOfSightController.SetSightConeSize ();

        float timer = 0;
        while (timer < 20) {
            if (Mathf.Floor (timer % 5) == 0) {
                Vector2 rand = Random.insideUnitCircle * 10;
                Vector3 targetPos = transform.position + new Vector3 (rand.x, 0, rand.y);
                agent.SetDestination (targetPos);
                
                transform.LookAt (targetPos);
            }
            timer += Time.deltaTime;
            yield return 0f;
        }
        currentState = WraithState.Idle;
    }

    private void OnCollisionEnter (Collision collision) {
        if (collision.collider.gameObject.CompareTag ("Player")) {
            Timing.KillAllCoroutines ();
            SceneManager.LoadScene ("EndGame");
        }
    }

    #region Properties
    public WraithState CurrentState {
        get {
            return currentState;
        }

        set {
            currentState = value;
        }
    }
    #endregion
}
